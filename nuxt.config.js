const pkg = require('./package');
require('dotenv').config();
const webpack = require('webpack');
const client = require('./plugins/contentful');


module.exports = {
  //mode: 'spa',
  /*
  ** Headers of the page
  */
  runtimerCompiler: true,
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    /* link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ] */
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#0000ff' },
  /*
  ** Global CSS
  */
  css: [
    "bootstrap/dist/css/bootstrap.css",
    "~/assets/scss/main.scss",
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    "~/plugins/bootstrap.js",
    "~/plugins/contentful.js",
    "~/plugins/moment.js",
    /* {
      src: "~/plugins/ga.js",
      ssr: false
    } */
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/markdownit',
    'nuxt-svg-loader',
    /* ['nuxt-facebook-pixel-module', {
      track: 'PageView',
      pixelId: 'FACEBOOK_PIXEL_ID',
      disabled: false
    }], */
  ],
  markdownit: {
    injected: true,
    preset: 'default',
    linkify: true,
    breaks: true,
    use: [
      'markdown-it-div',
      'markdown-it-attrs'
    ]
  },
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  router: {
    /* extendRoutes(routes, resolve) {


      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'pages/_slug/index.vue')
      })
    } */
    base: '/'
  },


  /* router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'pages/_slug/index.vue')
      })
    }
  }, */

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
   vendor: ['jquery', 'bootstrap'],
   plugins: [
    // set shortcuts as global for bootstrap
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  ],
   extend: (config, ctx) => {
    config.resolve.alias['vue'] = 'vue/dist/vue.common'
    const svgRule = config.module.rules.find(rule => rule.test.test('.svg'));

    svgRule.test = /\.(png|jpe?g|gif|webp)$/;

    config.module.rules.push({
      test: /\.svg$/,
      loader: 'vue-svg-loader',
    });
  }
  },
  generate: {
    /* routes() {
      return client
      .getEntries({content_type:'ostaleStrane'})
      .then(entries => {
        return entries.items.map(entry => {
          return {
            route: entry.fields.slug,
            payload: entry
          }
        })
      })
    }  */
    routes () {
      return Promise.all([
        client.getEntries({
          'content_type': 'ostaleStrane'
        }),
        client.getEntries({
          'content_type': 'post'
        })
      ])
        .then(([pageEntries, blogEntries]) => {
          return [
            ...pageEntries.items.map(entry => `${entry.fields.slug}`),
            ...blogEntries.items.map(entry => `/blog/${entry.fields.slug}`),
          ]
        }).catch((err) => {
          if (err.response.status === 404) {
            return this.$nuxt.error({ statusCode: 404, message: err.message })
          }
        })
    },
    dir: 'public'
  }
};
