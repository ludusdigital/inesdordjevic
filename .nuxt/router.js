import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _4c0f595c = () => interopDefault(import('../pages/error.vue' /* webpackChunkName: "pages/error" */))
const _d39b95c8 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _22b6eb57 = () => interopDefault(import('../pages/_slug/index.vue' /* webpackChunkName: "pages/_slug/index" */))
const _f54614f8 = () => interopDefault(import('../pages/_slug/_post.vue' /* webpackChunkName: "pages/_slug/_post" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/error",
    component: _4c0f595c,
    name: "error"
  }, {
    path: "/",
    component: _d39b95c8,
    name: "index"
  }, {
    path: "/:slug",
    component: _22b6eb57,
    name: "slug"
  }, {
    path: "/:slug/:post",
    component: _f54614f8,
    name: "slug-post"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
